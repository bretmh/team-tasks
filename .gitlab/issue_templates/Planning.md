# Capacity

List any noteworthy PTO that may impact the capacity for the planned milestone

# Planning

Indicate major efforts by linking to the appropriate epic. Under each epic indicate who is focusing on these efforts and list the individual issues assigned for this milestone. These will drive which issues are marked as ~Deliverable.  An example of the layout is below

## Infradev Issues
Infradev issues take priority. Review the links below to determine if there are any infradev issues that are not being addressed and need to be prioritized for this milestone
- [Infradev Reports](https://gitlab.com/gitlab-org/infradev-reports)
- [Issues](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&state=opened&label_name[]=group%3A%3Adatabase&label_name[]=infradev) with `group::database` and `infradev` labels

## Sharding Blocker Issues
In order to assist with the database scalability efforts outlined in [Decompose GitLab's database to improve scalability](https://gitlab.com/groups/gitlab-org/-/epics/6168) we need to prioritize issues labeled with ~group::database and ~sharding-blocker 
- [Issues](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&state=opened&label_name[]=group%3A%3Adatabase&label_name[]=sharding-blocker)

## Boards
- [Triage Board](https://gitlab.com/groups/gitlab-org/-/boards/2305765?scope=all&utf8=%E2%9C%93&label_name[]=database%3A%3Atriage)
- [Validation Board](https://gitlab.com/groups/gitlab-org/-/boards/2305758?scope=all&utf8=%E2%9C%93&label_name[]=group%3A%3Adatabase&label_name[]=database%3A%3Avalidation)
- [Build Board](https://gitlab.com/groups/gitlab-org/-/boards/1324138?&label_name[]=database%3A%3Aactive&label_name[]=group%3A%3Adatabase)

### [Epic Title](http://placeholder.com)

Who: @craig-gomes

Issues
- Issue link 1
- Issue link 2 

**Reminder**
Set ~"Deliverable" as the plan for the milestone solidifies.  Review during the milestone

/label ~"group::database" ~"devops::enablement"
/assign @craig-gomes @fzimmer @iroussos

cc @gitlab-org/database-team @ankelly @cdu1
